﻿namespace KadSys.Repository.EntityFramework
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using Abstractions;

    public class Repository<TEntity, TKey> : IRepository<TEntity, TKey> where TEntity : class, IEntity<TKey>
    {
        private readonly DbContext _dbContext;
        private readonly DbSet<TEntity> _set;

        public Repository(DbContext dbContext)
        {
            if (dbContext == null)
                throw new ArgumentNullException(nameof(dbContext));

            _dbContext = dbContext;
            _set = _dbContext.Set<TEntity>();
        }

        public TEntity Add(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity), "Entity can not be null!");
            }

            if (_set.Find(entity.Id) != null)
            {
                throw new ArgumentException("One or more entities already exist!");
            }

            if (_dbContext.Entry(entity).State == EntityState.Detached)
            {
                _set.Attach(entity);
            }

            _dbContext.Entry(entity).State = EntityState.Added;
            _dbContext.SaveChanges();
            return entity;
        }

        public IEnumerable<TEntity> AddRange(IEnumerable<TEntity> entities)
        {
            if (entities == null)
            {
                throw new ArgumentOutOfRangeException($"entities", "Enities can not be null or empty!");
            }

            var entityList = entities as IList<TEntity> ?? entities.ToList();
            if (entities == null || !entityList.Any())
            {
                throw new ArgumentOutOfRangeException($"entities", "Enities can not be null or empty!");
            }

            foreach (var entity in entityList)
            {
                if (!entity.Id.Equals(default(TKey)))
                {
                    throw new ArgumentException("One or more entities already exist!");
                }

                if (_dbContext.Entry(entity).State == EntityState.Detached)
                {
                    _set.Attach(entity);
                }

                _dbContext.Entry(entity).State = EntityState.Added;
            }

            _dbContext.SaveChanges();

            return entityList;
        }

        public IEnumerable<TEntity> All(Func<TEntity, bool> predicate)
        {
            return _set.Where(predicate).ToList();
        }

        public TEntity Single(Func<TEntity, bool> predicate)
        {
            return _set.Single(predicate);
        }

        public TEntity SingleOrNull(Func<TEntity, bool> predicate)
        {
            return _set.SingleOrDefault(predicate);
        }

        public TEntity First(Func<TEntity, bool> predicate)
        {
            return _set.First(predicate);
        }

        public TEntity FirstOrNull(Func<TEntity, bool> predicate)
        {
            return _set.FirstOrDefault(predicate);
        }

        public bool Any(Func<TEntity, bool> predicate)
        {
            return _set.Any(predicate);
        }

        public void Update(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            if (_set.Find(entity.Id) == null)
            {
                throw new ArgumentException("Entity does not exist!");
            }

            if (_dbContext.Entry(entity).State == EntityState.Detached)
            {
                _set.Attach(entity);
            }

            _dbContext.Entry(entity).State = EntityState.Modified;

            _dbContext.SaveChanges();
        }

        public void UpdateRange(IEnumerable<TEntity> entities)
        {
            if (entities == null)
            {
                throw new ArgumentOutOfRangeException($"entities", "Enities can not be null or empty!");
            }

            var entityList = entities as IList<TEntity> ?? entities.ToList();
            if (entities == null || !entityList.Any())
            {
                throw new ArgumentOutOfRangeException($"entities", "Enities can not be null or empty!");
            }

            foreach (var entity in entityList)
            {
                if (_set.Find(entity.Id) == null)
                {
                    throw new ArgumentException("One or more entities do not exist!");
                }

                if (_dbContext.Entry(entity).State == EntityState.Detached)
                {
                    _set.Attach(entity);
                }
                _dbContext.Entry(entity).State = EntityState.Modified;
            }
            _dbContext.SaveChanges();
        }

        public void Remove(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            if (_set.Find(entity.Id) == null)
            {
                throw new ArgumentException("Enitiy does not exist.");
            }

            if (_dbContext.Entry(entity).State == EntityState.Detached)
            {
                _set.Attach(entity);
            }

            _dbContext.Entry(entity).State = EntityState.Deleted;

            _dbContext.SaveChanges();
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            if (entities == null)
            {
                throw new ArgumentOutOfRangeException(nameof(entities), "is null");
            }

            var entityList = entities as IList<TEntity> ?? entities.ToList();

            if (!entityList.Any())
            {
                throw new ArgumentOutOfRangeException(nameof(entities), "is empty");
            }

            foreach (var entity in entityList)
            {
                if (_set.Find(entity.Id) == null)
                {
                    throw new ArgumentException("One or more entities do not exist.");
                }

                if (_dbContext.Entry(entity).State == EntityState.Detached)
                {
                    _set.Attach(entity);
                }

                _dbContext.Entry(entity).State = EntityState.Deleted;
            }

            _dbContext.SaveChanges();
        }
    }

    public class Repository<TEntity> : Repository<TEntity, long>, IRepository<TEntity>
        where TEntity : class, IEntity
    {
        public Repository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}