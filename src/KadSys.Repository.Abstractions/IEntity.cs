﻿namespace KadSys.Repository.Abstractions
{
    /// <summary>
    ///     Represents an IEntity with an Id property.
    /// </summary>
    /// <typeparam name="TKey">The type of the key.</typeparam>
    public interface IEntity<TKey>
    {
        /// <summary>
        ///     Gets or sets the identifier.
        /// </summary>
        TKey Id { get; set; }
    }

    /// <summary>
    ///     Represents an entity with an Id property of type <see cref="long" />.
    /// </summary>
    public interface IEntity : IEntity<long>
    {
    }
}