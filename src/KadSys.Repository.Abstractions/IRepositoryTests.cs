﻿namespace KadSys.Repository.Abstractions
{
    /// <summary>
    ///     Defines methods which should be implemented to test the public repository
    ///     interfaces.
    /// </summary>
    public interface IRepositoryTests
    {
        void Add_should_create_and_new_entity();
        void Add_should_throw_ArgumentNullException_if_entity_is_null();
        void Add_should_throw_ArgumentException_if_entity_already_exist();
        void AddRange_should_create_multiple_entities();
        void AddRange_should_throw_ArgumentOutOfRangeException_if_null_or_non_entities_are_given();
        void AddRange_should_throw_ArgumentException_if_one_or_more_entities_already_exist();
        void All_should_return_entities_satisfying_the_predicate();
        void All_should_throw_ArgumentNullException_when_predicate_is_null();
        void Single_should_return_an_entity_satisfying_the_predicate();
        void Single_should_throw_InvalidOperationException_if_no_entity_satisfies_the_predicate();
        void Single_should_throw_InvalidOperationException_if_more_than_one_entities_satisfies_the_predicate();
        void Single_should_throw_ArgumentNullException_when_predicate_is_nullTest();
        void SingleOrNull_should_return_an_entity_satisfying_the_predicate();
        void SingleOrNull_should_return_null_if_no_entity_satisfies_the_predicate();
        void SingleOrNull_should_throw_InvalidOperationException_if_more_than_one_entities_satisfies_the_predicate();
        void SingleOrNull_should_throw_ArgumentNullException_when_predicate_is_null();
        void First_should_return_the_first_entity_satisfying_the_predicate();
        void First_should_throw_InvalidOperationException_if_no_entity_satisfies_the_predicate();
        void First_should_throw_ArgumentNullException_when_predicate_is_null();
        void FirstOrNull_should_return_the_first_entity_satisfying_the_predicate();
        void FirstOrNull_should_return_null_if_no_entity_satisfies_the_predicate();
        void FirstOrNull_should_throw_ArgumentNullException_when_predicate_is_null();
        void Any_should_throw_ArgumentNullException_when_predicate_is_null();
        void Any_should_return_true_if_any_entity_which_satisfies_the_predicate_exist();
        void Any_should_return_false_if_no_entity_which_satisfies_the_predicate_exist();
        void Update_should_change_an_entity();
        void Update_should_throw_ArgumentException_if_entity_does_not_exist();
        void Update_should_throw_ArgumentNullException_if_entity_is_null();
        void UpdateRange_should_change_the_entities();
        void UpdateRange_should_throw_ArgumentException_if_one_ore_more_entities_do_not_exist();
        void UpdateRange_should_throw_ArgumentOutOfRangeException_if_null_or_non_entities_are_given();
        void Remove_an_entity_from();
        void Remove_should_throw_ArgumentNullException_if_entity_is_null();
        void Remove_should_throw_ArgumentException_if_entity_does_not_exist();
        void RemoveRange_should_remove_all_entities();
        void RemoveRange_should_throw_ArgumentException_if_one_or_more_entities_do_not_exist();
        void RemoveRange_should_throw_ArgumentOutOfRangeException_if_null_or_non_entities_are_given();
    }
}