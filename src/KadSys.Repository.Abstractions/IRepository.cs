﻿namespace KadSys.Repository.Abstractions
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///     Represents a generic repository which provides a simple data access for underlying store.
    /// </summary>
    /// <typeparam name="TEntity">The entity type.</typeparam>
    /// <typeparam name="TKey">The id type of the entity.</typeparam>
    public interface IRepository<TEntity, TKey>
        where TEntity : IEntity<TKey>
    {
        /// <summary>
        ///     Adds an entity to the store.
        /// </summary>
        /// <param name="entity">The entity to add.</param>
        /// <returns>The added entity.</returns>
        /// <exception cref="ArgumentException">If the entity already exist or key is not the default value.</exception>
        /// <exception cref="ArgumentNullException">If entity is null.</exception>
        TEntity Add(TEntity entity);

        /// <summary>
        ///     Adds multiple entities to the store.
        /// </summary>
        /// <param name="entities">The entities to add.</param>
        /// <returns>The added entities.</returns>
        /// <exception cref="ArgumentException">If one or more entities already exist.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If entities is null or empty.</exception>
        IEnumerable<TEntity> AddRange(IEnumerable<TEntity> entities);

        /// <summary>
        ///     Retrieves all entities satisfying the predicate.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <exception cref="ArgumentNullException">If predicate is null.</exception>
        /// <returns>Entities satisfying the predicate.</returns>
        IEnumerable<TEntity> All(Func<TEntity, bool> predicate);

        /// <summary>
        ///     Retrieve a single entity satisfying the predicate.
        /// </summary>
        /// <param name="predicate">The predicate to satisfy.</param>
        /// <returns>The entity satisfying the predicate.</returns>
        /// <exception cref="ArgumentNullException">If predicate is null.</exception>
        /// <exception cref="InvalidOperationException">If non or more than one entity can be found.</exception>
        TEntity Single(Func<TEntity, bool> predicate);

        /// <summary>
        ///     Retrieve a single entity satisfying the predicate.
        /// </summary>
        /// <param name="predicate">The predicate to satisfy.</param>
        /// <returns>The entity satisfying the predicate or null if non entity can be found.</returns>
        /// <exception cref="ArgumentNullException">If predicate is null.</exception>
        /// <exception cref="InvalidOperationException">If more than one entity can be found.</exception>
        TEntity SingleOrNull(Func<TEntity, bool> predicate);

        /// <summary>
        ///     Retrieve the first entity satisfying the predicate.
        /// </summary>
        /// <param name="predicate">The predicate to satisfy.</param>
        /// <returns>First entity satisfying the predicate.</returns>
        /// <exception cref="ArgumentNullException">If predicate is null.</exception>
        /// <exception cref="InvalidOperationException">If non entity can be found.</exception>
        TEntity First(Func<TEntity, bool> predicate);

        /// <summary>
        ///     Retrieve the first entity satisfying the predicate.
        /// </summary>
        /// <param name="predicate">The predicate to satisfy.</param>
        /// <exception cref="ArgumentNullException">If predicate is null.</exception>
        /// <returns>First entity satisfying the predicate or null if non entity can be found.</returns>
        TEntity FirstOrNull(Func<TEntity, bool> predicate);

        /// <summary>
        ///     Check if an entity which satisfies the predicate exist.
        /// </summary>
        /// <param name="predicate">The predicate to satisfy.</param>
        /// <exception cref="ArgumentNullException">If predicate is null.</exception>
        /// <returns>True if an entity which satisfies the predicate exist otherwise false.</returns>
        bool Any(Func<TEntity, bool> predicate);

        /// <summary>
        ///     Updates an entity in the store.
        /// </summary>
        /// <param name="entity">Entity to update.</param>
        /// <exception cref="ArgumentNullException">If entity is null.</exception>
        /// <exception cref="ArgumentException">If entity does not exist</exception>
        void Update(TEntity entity);

        /// <summary>
        ///     Update multiple entities in the store.
        /// </summary>
        /// <param name="entities">Entities to update</param>
        /// <exception cref="ArgumentOutOfRangeException">If entities is null or empty.</exception>
        /// <exception cref="ArgumentException">If one ore more entities do no exist.</exception>
        void UpdateRange(IEnumerable<TEntity> entities);

        /// <summary>
        ///     Removes an entity from the store.
        /// </summary>
        /// <param name="entity"></param>
        /// <exception cref="ArgumentNullException">If entity is null.</exception>
        /// <exception cref="ArgumentException">If entity does not exist.</exception>
        void Remove(TEntity entity);

        /// <summary>
        ///     Removes multiple entities from the store.
        /// </summary>
        /// <param name="entities">Entities to remove.</param>
        /// <exception cref="ArgumentOutOfRangeException">If entities is null or empty.</exception>
        /// <exception cref="ArgumentException">If one or more entities do not exist.</exception>
        void RemoveRange(IEnumerable<TEntity> entities);
    }

    /// <summary>
    ///     Represents a generic repository which provides a simple data access for underlying store.
    /// </summary>
    /// <typeparam name="TEntity">The entity type with key type <see cref="long" /></typeparam>
    public interface IRepository<TEntity> : IRepository<TEntity, long>
        where TEntity : IEntity
    {
    }
}