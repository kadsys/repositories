param(
    [string]$LocalNuGetPackagesDir=$env:LOCAL_NUGET_PACKAGES, 
    [string]$Verbosity='normal',
    [switch]$UseDebugBuild)

if(!$LocalNuGetPackagesDir){
    Write-Host 'Set LOCAL_NUGET_PACKAGES environment variable or pass the location of local NuGet repository as parameter!' -ForegroundColor Red
    exit
}

if($Verbosity -notmatch '^(normal)|(quiet)|(detailed)$'){
    Write-Host "Invalid verbosity option: $Verbosity"
    Write-Host "Valid options are: normal|quiet|detailed"
    exit
}

$dirPart = if($UseDebugBuild) {'Debug'} else {'Release'}

$packages = ls -Filter '*.nuspec' -Name | %{$_.Replace('.nuspec','')} | %{ls -Recurse -Path '..' -Filter "$_*.nupkg"} | where Directory $dirPart -Match
$packages | %{ NuGet add $_.FullName -Source $LocalNuGetPackagesDir -Verbosity $Verbosity}