namespace KadSys.Repository.EntityFrameworkTest
{
    using System.Data.Common;
    using System.Data.Entity;

    internal class TestDbContext : DbContext
    {
        public TestDbContext()
        {
        }

        public TestDbContext(DbConnection existingConnection)
            : base(existingConnection, true)
        {
        }

        public IDbSet<TestEntity> TestEntities { get; set; }
    }
}