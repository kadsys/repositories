﻿namespace KadSys.Repository.EntityFrameworkTest
{
    using Abstractions;

    internal class TestEntity : IEntity
    {
        public string Name { get; set; }
        public long Id { get; set; }
    }
}