namespace KadSys.Repository.EntityFrameworkTest
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Abstractions;
    using Effort;
    using EntityFramework;
    using Xunit;

    public class RepositoryTests : IDisposable, IRepositoryTests
    {
        private readonly TestDbContext _dbContext;
        private IRepository<TestEntity, long> _repository;

        public RepositoryTests()
        {
            var connection = DbConnectionFactory.CreateTransient();
            _dbContext = new TestDbContext(connection);
            _dbContext.Database.CreateIfNotExists();
            _repository = new Repository<TestEntity, long>(_dbContext);
        }

        public void Dispose()
        {
            _repository = null;
            _dbContext.Dispose();
            GC.SuppressFinalize(this);
        }

        [Fact]
        public void Add_should_create_and_new_entity()
        {
            Assert.False(_dbContext.TestEntities.Any());

            var newEntity = new TestEntity {Name = "Test"};
            Assert.Equal(default(long), newEntity.Id);

            newEntity = _repository.Add(newEntity);
            Assert.NotEqual(default(long), newEntity.Id);
            Assert.Equal("Test", newEntity.Name);

            Assert.Equal(newEntity, _dbContext.TestEntities.First());
        }

        [Fact]
        public void Add_should_throw_ArgumentNullException_if_entity_is_null()
        {
            Assert.Throws<ArgumentNullException>(() => _repository.Add(null));
        }

        [Fact]
        public void Add_should_throw_ArgumentException_if_entity_already_exist()
        {
            var entity = new TestEntity {Name = "test"};
            Assert.Equal(default(long), entity.Id);

            entity = _dbContext.TestEntities.Add(entity);
            _dbContext.SaveChanges();

            Assert.NotEqual(default(long), entity.Id);

            Assert.Throws<ArgumentException>(() => _repository.Add(entity));
        }

        [Fact]
        public void AddRange_should_create_multiple_entities()
        {
            var entities = new[] {1, 2, 3}.Select(i => new TestEntity {Name = $"entity {i}"}).ToList();
            foreach (var entity in entities)
            {
                Assert.Equal(default(long), entity.Id);
            }
            Assert.Equal(0, _dbContext.TestEntities.Count());

            var addedEntities = _repository.AddRange(entities);
            Assert.Equal(entities.Count, addedEntities.Count());
            Assert.Equal(entities.Count, _dbContext.TestEntities.Count());

            foreach (var entity in entities)
            {
                Assert.NotEqual(default(long), entity.Id);
            }
        }

        [Fact]
        public void AddRange_should_throw_ArgumentOutOfRangeException_if_null_or_non_entities_are_given()
        {
            Assert.Equal(0, _dbContext.TestEntities.Count());
            Assert.Throws<ArgumentOutOfRangeException>(() => _repository.AddRange(null));
            Assert.Throws<ArgumentOutOfRangeException>(() => _repository.AddRange(new List<TestEntity>()));
            Assert.Equal(0, _dbContext.TestEntities.Count());
        }

        [Fact]
        public void AddRange_should_throw_ArgumentException_if_one_or_more_entities_already_exist()
        {
            Assert.Equal(0, _dbContext.TestEntities.Count());
            var existEntity2 = _dbContext.TestEntities.Add(new TestEntity {Name = "entity 2"});
            var existEntity4 = _dbContext.TestEntities.Add(new TestEntity {Name = "entity 4"});
            _dbContext.SaveChanges();
            Assert.Equal(2, _dbContext.TestEntities.Count());

            var entities = new List<TestEntity>
            {
                new TestEntity {Name = "entity 1"},
                existEntity2,
                new TestEntity {Name = "entity 3"},
                existEntity4
            };

            Assert.Throws<ArgumentException>(() => _repository.AddRange(entities));
            Assert.Equal(2, _dbContext.TestEntities.Count());
        }

        [Fact]
        public void All_should_return_entities_satisfying_the_predicate()
        {
            var entities = new[] {1, 2, 3}.Select(i => new TestEntity {Name = $"entity {i}"}).ToList();
            foreach (var entity in entities)
            {
                _dbContext.TestEntities.Add(entity);
            }
            _dbContext.SaveChanges();
            Assert.Equal(entities.Count, _dbContext.TestEntities.Count());

            var actEntities = _repository.All(entity => true).ToList();
            Assert.Equal(entities.Count, actEntities.Count);
            Assert.Equal(entities, actEntities);

            actEntities = _repository.All(entity => entity.Id > 1).ToList();
            Assert.Equal(entities.Count(e => e.Id > 1), actEntities.Count);

            actEntities = _repository.All(entity => false).ToList();
            Assert.Equal(0, actEntities.Count);
        }

        [Fact]
        public void All_should_throw_ArgumentNullException_when_predicate_is_null()
        {
            Assert.Throws<ArgumentNullException>(() => _repository.All(null));
        }

        [Fact]
        public void Single_should_return_an_entity_satisfying_the_predicate()
        {
            var entities = new[] {1, 2, 3}.Select(i => new TestEntity {Name = $"entity {i}"}).ToList();
            foreach (var entity in entities)
            {
                _dbContext.TestEntities.Add(entity);
            }
            _dbContext.SaveChanges();

            var actEntity = _repository.Single(e => e.Name == "entity 2");
            Assert.NotNull(actEntity);
        }

        [Fact]
        public void Single_should_throw_InvalidOperationException_if_no_entity_satisfies_the_predicate()
        {
            Assert.Throws<InvalidOperationException>(() => _repository.Single(e => e.Name == "not existed entity"));
        }

        [Fact]
        public void Single_should_throw_InvalidOperationException_if_more_than_one_entities_satisfies_the_predicate
            ()
        {
            var entities = new[] {1, 2, 3}.Select(i => new TestEntity {Name = $"entity {i}"}).ToList();
            foreach (var entity in entities)
            {
                _dbContext.TestEntities.Add(entity);
            }
            _dbContext.SaveChanges();

            Assert.Throws<InvalidOperationException>(() => _repository.Single(e => e.Id > 1));
        }

        [Fact]
        public void Single_should_throw_ArgumentNullException_when_predicate_is_nullTest()
        {
            Assert.Throws<ArgumentNullException>(() => _repository.Single(null));
        }

        [Fact]
        public void SingleOrNull_should_return_an_entity_satisfying_the_predicate()
        {
            var entities = new[] {1, 2, 3}.Select(i => new TestEntity {Name = $"entity {i}"}).ToList();
            foreach (var entity in entities)
            {
                _dbContext.TestEntities.Add(entity);
            }
            _dbContext.SaveChanges();

            var actEntity = _repository.SingleOrNull(e => e.Name == "entity 2");
            Assert.NotNull(actEntity);
        }

        [Fact]
        public void SingleOrNull_should_return_null_if_no_entity_satisfies_the_predicate()
        {
            var entity = _repository.SingleOrNull(e => e.Name == "not existed entity");
            Assert.Null(entity);
        }

        [Fact]
        public void
            SingleOrNull_should_throw_InvalidOperationException_if_more_than_one_entities_satisfies_the_predicate()
        {
            var entities = new[] {1, 2, 3}.Select(i => new TestEntity {Name = $"entity {i}"}).ToList();
            foreach (var entity in entities)
            {
                _dbContext.TestEntities.Add(entity);
            }
            _dbContext.SaveChanges();

            Assert.Throws<InvalidOperationException>(() => _repository.Single(e => e.Id > 1));
        }

        [Fact]
        public void SingleOrNull_should_throw_ArgumentNullException_when_predicate_is_null()
        {
            Assert.Throws<ArgumentNullException>(() => _repository.SingleOrNull(null));
        }

        [Fact]
        public void First_should_return_the_first_entity_satisfying_the_predicate()
        {
            var entities = new[] {1, 2, 3}.Select(i => new TestEntity {Name = $"entity {i}"}).ToList();
            foreach (var entity in entities)
            {
                _dbContext.TestEntities.Add(entity);
            }
            _dbContext.SaveChanges();

            var actEntity = _repository.First(e => e.Id > 1);
            Assert.Equal(2, actEntity.Id);
            Assert.Equal("entity 2", actEntity.Name);
        }

        [Fact]
        public void First_should_throw_InvalidOperationException_if_no_entity_satisfies_the_predicate()
        {
            Assert.Throws<InvalidOperationException>(() => _repository.First(e => e.Name == "not existed entity"));
        }

        [Fact]
        public void First_should_throw_ArgumentNullException_when_predicate_is_null()
        {
            Assert.Throws<ArgumentNullException>(() => _repository.First(null));
        }

        [Fact]
        public void FirstOrNull_should_return_the_first_entity_satisfying_the_predicate()
        {
            var entities = new[] {1, 2, 3}.Select(i => new TestEntity {Name = $"entity {i}"}).ToList();
            foreach (var entity in entities)
            {
                _dbContext.TestEntities.Add(entity);
            }
            _dbContext.SaveChanges();

            var actEntity = _repository.FirstOrNull(e => e.Id > 1);
            Assert.Equal(2, actEntity.Id);
            Assert.Equal("entity 2", actEntity.Name);
        }

        [Fact]
        public void FirstOrNull_should_return_null_if_no_entity_satisfies_the_predicate()
        {
            var first = _repository.FirstOrNull(e => e.Name == "not existed entity");
            Assert.Null(first);
        }

        [Fact]
        public void FirstOrNull_should_throw_ArgumentNullException_when_predicate_is_null()
        {
            Assert.Throws<ArgumentNullException>(() => _repository.FirstOrNull(null));
        }

        [Fact]
        public void Any_should_throw_ArgumentNullException_when_predicate_is_null()
        {
            Assert.Throws<ArgumentNullException>(() => _repository.Any(null));
        }

        [Fact]
        public void Any_should_return_true_if_any_entity_which_satisfies_the_predicate_exist()
        {
            var entities = new[] {1, 2, 3}.Select(i => new TestEntity {Name = $"entity {i}"}).ToList();
            foreach (var entity in entities)
            {
                _dbContext.TestEntities.Add(entity);
            }
            _dbContext.SaveChanges();

            Assert.True(_repository.Any(e => e.Id > 1), "There should be two entities which satisfies the predicate.");
        }

        [Fact]
        public void Any_should_return_false_if_no_entity_which_satisfies_the_predicate_exist()
        {
            Assert.False(_repository.Any(e => e.Id > 1), "There should be no entities which satisfies the predicate.");
        }

        [Fact]
        public void Update_should_change_an_entity()
        {
            var entity = new TestEntity {Name = "test entity"};
            _dbContext.TestEntities.Add(entity);
            _dbContext.SaveChanges();

            Assert.Equal(1, _dbContext.TestEntities.Count());
            Assert.Equal("test entity", entity.Name);

            entity.Name = "test entity updated";

            _repository.Update(entity);

            var updatedEntity = _dbContext.TestEntities.Single();
            Assert.Equal(entity, updatedEntity);
            Assert.Equal("test entity updated", updatedEntity.Name);
        }

        [Fact]
        public void Update_should_throw_ArgumentException_if_entity_does_not_exist()
        {
            var entity = new TestEntity {Name = "test entity"};

            Assert.Equal(0, _dbContext.TestEntities.Count());

            entity.Name = "test entity updated";

            Assert.Throws<ArgumentException>(() => _repository.Update(entity));
        }

        [Fact]
        public void Update_should_throw_ArgumentNullException_if_entity_is_null()
        {
            Assert.Throws<ArgumentNullException>(() => _repository.Update(null));
        }

        [Fact]
        public void UpdateRange_should_change_the_entities()
        {
            var entities = new[] {1, 2, 3}.Select(i => new TestEntity {Name = $"entity {i}"}).ToList();
            foreach (var entity in entities)
            {
                _dbContext.TestEntities.Add(entity);
            }
            _dbContext.SaveChanges();

            Assert.Equal(entities, _dbContext.TestEntities);

            foreach (var entity in entities)
            {
                entity.Name = $"{entity.Name} updated";
            }
            _repository.UpdateRange(entities);

            Assert.Equal(entities, _dbContext.TestEntities.Where(t => t.Name.Contains("updated")).ToList());
        }

        [Fact]
        public void UpdateRange_should_throw_ArgumentException_if_one_ore_more_entities_do_not_exist()
        {
            var entities = new[] {1, 2, 3}.Select(i => new TestEntity {Name = $"entity {i}"}).ToList();
            foreach (var entity in entities)
            {
                _dbContext.TestEntities.Add(entity);
            }
            _dbContext.SaveChanges();

            entities.Add(new TestEntity {Name = "not existed entity"});

            Assert.Equal(3, _dbContext.TestEntities.Count());
            Assert.Throws<ArgumentException>(() => _repository.UpdateRange(entities));
            Assert.Equal(3, _dbContext.TestEntities.Count());
        }

        [Fact]
        public void UpdateRange_should_throw_ArgumentOutOfRangeException_if_null_or_non_entities_are_given()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => _repository.UpdateRange(null));
            Assert.Throws<ArgumentOutOfRangeException>(() => _repository.UpdateRange(new List<TestEntity>()));
        }

        [Fact]
        public void Remove_an_entity_from()
        {
            var entities = new[] {1, 2, 3}.Select(i => new TestEntity {Name = $"entity {i}"}).ToList();
            foreach (var entity in entities)
            {
                _dbContext.TestEntities.Add(entity);
            }
            _dbContext.SaveChanges();

            var entityToRemove = _dbContext.TestEntities.SingleOrDefault(e => e.Name == "entity 2");

            _repository.Remove(entityToRemove);
            Assert.Equal(entities.Except(new[] {entityToRemove}), _dbContext.TestEntities.ToList());
        }

        [Fact]
        public void Remove_should_throw_ArgumentNullException_if_entity_is_null()
        {
            Assert.Throws<ArgumentNullException>(() => _repository.Remove(null));
        }

        [Fact]
        public void Remove_should_throw_ArgumentException_if_entity_does_not_exist()
        {
            Assert.Throws<ArgumentException>(() => _repository.Remove(new TestEntity {Name = "not existed entity"}));
        }

        [Fact]
        public void RemoveRange_should_remove_all_entities()
        {
            var entities = new[] {1, 2, 3}.Select(i => new TestEntity {Name = $"entity {i}"}).ToList();
            foreach (var entity in entities)
            {
                _dbContext.TestEntities.Add(entity);
            }
            _dbContext.SaveChanges();

            var entitiesToRemove = _dbContext.TestEntities.ToList();

            _repository.RemoveRange(entitiesToRemove);
            Assert.Equal(0, _dbContext.TestEntities.Count());
        }

        [Fact]
        public void RemoveRange_should_throw_ArgumentException_if_one_or_more_entities_do_not_exist()
        {
            var entities = new[] {1, 2, 3}.Select(i => new TestEntity {Name = $"entity {i}"}).ToList();
            foreach (var entity in entities)
            {
                _dbContext.TestEntities.Add(entity);
            }
            _dbContext.SaveChanges();


            var entitiesToRemove = _dbContext.TestEntities.ToList();

            entitiesToRemove.Add(new TestEntity {Name = "not existed entity"});

            Assert.Throws<ArgumentException>(() => _repository.RemoveRange(entitiesToRemove));
            Assert.Equal(3, _dbContext.TestEntities.Count());
        }

        [Fact]
        public void RemoveRange_should_throw_ArgumentOutOfRangeException_if_null_or_non_entities_are_given()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => _repository.RemoveRange(null));
            Assert.Throws<ArgumentOutOfRangeException>(() => _repository.RemoveRange(new List<TestEntity>()));
        }
    }
}